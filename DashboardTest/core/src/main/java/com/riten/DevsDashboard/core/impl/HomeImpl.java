package com.riten.DevsDashboard.core.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.object.db.OObjectDatabaseTx;

public class HomeImpl {
	private static final Logger LOGGER = LoggerFactory.getLogger(HomeImpl.class);

	public HomeImpl() {
	}

	private void fetchUserDetails(String userName) {
		//creating DB instance in order to connect to Orient DB (JAVA API)
        OObjectDatabaseTx db = getDatabase();
	}
	
	private static OObjectDatabaseTx getDatabase() {
		// create a database in memory
//        OObjectDatabaseTx db = new OObjectDatabaseTx("memory:data").create();

        // create a database on disk
        OObjectDatabaseTx db = new OObjectDatabaseTx("plocal:data");
        if (db.exists()) {
        	db = new OObjectDatabaseTx("plocal:data").open("admin", "admin");
        } else {
        	db.create();
        }
		return db;
	}
}
