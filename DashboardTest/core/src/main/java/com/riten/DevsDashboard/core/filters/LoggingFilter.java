/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.riten.DevsDashboard.core.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.felix.scr.annotations.sling.SlingFilter;
import org.apache.felix.scr.annotations.sling.SlingFilterScope;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Simple servlet filter component that logs incoming requests.
 */
@SlingFilter(order = -700, scope = SlingFilterScope.REQUEST)
public class LoggingFilter implements Filter {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response,
            final FilterChain filterChain) throws IOException, ServletException {

        final SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) request;
        final SlingHttpServletResponse slingResponse = (SlingHttpServletResponse) response;
        logger.debug("request for {}, with selector {}", slingRequest
                .getRequestPathInfo().getResourcePath(), slingRequest
                .getRequestPathInfo().getSelectorString());
        String requestURI = ((HttpServletRequest) request).getRequestURI();
        logger.info("requestURI>>>"+requestURI);
       
       /* String userIDSession = slingRequest.getAttribute(UtilConstants.UserID).toString();
        if(null!=userIDSession){
        	logger.info("userIDSession is not null>>>");
        	//reset the lifetime of cookie
        }else{
        	logger.info("userIDSession is null>>>");
        	//set the value in session as sessionOut to true
        	slingRequest.getSession().setAttribute("sessionOut", true);
        }*/
      /*  Cookie[] cookies = ((HttpServletRequest) request).getCookies();
        Cookie loginCookie = null;
    	if(cookies != null){
    	for(Cookie cookie : cookies){
    		if(cookie.getName().equals("userID")){
    			logger.info("Inside Filter, Cookie with userID exists>>");
    			loginCookie = cookie;
    			break;
    		}
    	}
    	}
    	if(loginCookie != null){
    		loginCookie.setMaxAge(15*60);
    		logger.info("Inside Logout Servlet, setting max-age to 15 mins.");
    		 //filterChain.doFilter(request, response);
    	}else{
    		slingResponse.sendRedirect("Login.html");
    	}*/
    	
    	filterChain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig filterConfig) {}

    @Override
    public void destroy() {}

}